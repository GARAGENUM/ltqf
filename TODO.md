# Desktop
- [x] lien logo navbar -> ne renvoie pas vers l'index
- [x] flèche up présente sur la page d'accueil pour rien
- [x] news page d'accueil -> défilement trop rapide
- [] lien fb et instagram
- [x] page compagnie -> paragraphes trop denses
- [] flèche up présente même avant le scroll
- [x] page spectacles -> flèche up non-visible
- [] page spectacles/un-apprentissage ->  pas de galerie
- [] page spectacles/single -> background de photo principal à mettre en noir
- [] page spectacles/single -> photo savoir trop haute
- [] page spectacles/le-secret -> rien dans En savoir plus 
- [x] page spectacles/single -> galeries avec des grandes photos portraits
- [] page spectacles/single -> galeries avec des photos pas centrées
- [] page spectacles/single -> galeries avec la dernière photo qu'on voie apparaitre au lieu d'un slide normal
- [] page spectacles/promenade-en-robe-de-chambre -> rien dans la galerie
- [x] page actions-culturelles -> hauteur des cartes pas uniforme
- [x] page partenaires -> agencement des logos soutien (sacd tout seul au milieu)
- [] page presse/ou-sont-les-ogres -> pas de lien
- [] page presse/outrages -> pas de lien
- [] page presse/fiancee-de-barbe-bleue -> pas de lien
- [] page presse/la-lettre -> pas de lien


# Remarques de Kahena et Pierre-Yves

- [] Ajouter le titre "Actualités" dans le overlay du home
- [] 