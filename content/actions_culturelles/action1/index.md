---
title: Dispositif ULIS
description: "Page de présentation d'une action culturelle de la compagnie Le Temps Qu'il Faut en milieu scolaire"
---

Actuellement, en partenariat avec le Théâtre National de La Coline, Kahena Saïghi mène un projet théâtre « inclusion inversée » avec le dispositif ULIS et une classe de 5ème classique du collège Jean-Pierre Timbaud à Bobigny, sur les thématiques liées à l’adolescence (Les corps qui changent, le rejet et l’acceptation de l’autre…)
