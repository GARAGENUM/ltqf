---
title: "Mentions Légales"
draft: false
description: "Page de présentation des mentions légales du site Internet ltqf.fr" 
subtitle: "Consultez nos mentions légales pour obtenir des informations sur l'entreprise, les droits d'auteur, la politique de confidentialité et plus."
canonical: "https://ltqf.fr/mentions_legales/"
mentions:
  - title: "Editeur du Site Web"
    message: |
      Ce site est édité par l'Association Arthages, association loi 1901 à but non lucratif.  
      8 rue des Plâtrières 75020 Paris  
      arthages.contact@gmail.com
  - title: "Directeur de la Publication"
    message: "Pierre-Yves Chapalain, en qualité d'auteur, acteur et metteur en scène, Association Le Temps Qu'il Faut."
  - title: "Hébergement"
    message: |
      Ce site est hébergé par Gandi. www.gandi.net  
      63-65 boulevard Masséna 75013 Paris  
      Tel +33 (0) 1 70.37.76.61  
      Fax +33. (0) 1 43 73 18 51  
      direction@gandi.net
  - title: "Propriété Intellectuelle"
    message: "L’ensemble de ce site relève de la législation française et internationale sur le droit d’auteur et la propriété intellectuelle. Tous les droits de reproduction sont réservés, y compris pour les documents téléchargeables et les représentations iconographiques et photographiques. La reproduction de tout ou partie de ce site sur un support électronique quel qu'il soit est formellement interdite sauf autorisation expresse du directeur de la publication."
  - title: "Données Personnelles et Analytics"
    message: "Le site Le Temps Qu'il Faut utilise Open Source Web Analytics pour le suivi anonyme des visites. Aucune donnée personnelle n'est collectée sans le consentement explicite des visiteurs. Pour plus d'informations sur notre politique de confidentialité et la gestion des données, veuillez nous contacter."
  - title: "Droits d'Auteur"
    message: "Les œuvres présentées sur ce site appartiennent à leurs auteurs respectifs. Toute utilisation, reproduction, diffusion, commercialisation, modification de toute ou partie du site, sans autorisation de l'Association Le Temps Qu'il Faut est prohibée et peut entraîner des actions et poursuites judiciaires telles que prévues par le code de la propriété intellectuelle et le code civil."
  - message: "Pour toute demande de renseignements ou d'autorisation, veuillez nous contacter cie@ltqf.fr"
---