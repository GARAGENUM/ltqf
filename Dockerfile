# Use an image that includes both Hugo and Nginx
FROM hugomods/hugo:0.111.3 AS builder
COPY . /src
WORKDIR /src
RUN hugo --minify --gc

FROM hugomods/hugo:nginx
#COPY --from=builder /src/public /usr/share/nginx/html
COPY --from=builder /src/public /site
# Default Nginx config should be sufficient for serving static content
