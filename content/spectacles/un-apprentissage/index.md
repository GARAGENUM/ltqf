---
title: "Un apprentissage"
subtitle: "Spectacle tout terrain créé le 11 novembre 2021 au Festival du TNB"
description: "Page de présentation du spectacle Un Apprentissage de la compagnie Le Temps Qu'il Faut"
canonical: "https://ltqf.fr/spectacles/un-apprentissage/"
date: "2024-01-01"
draft: false
credits: 
  - role: "Ecriture et mise en scène" 
    name: "Pierre-Yves Chapalain et Kahena Saïghi"
  - role: "Avec"
    name: "Pierre-Yves Chapalain"
  - role: "Production"
    name: 
      - "Compagnie Le Temps qu’il Faut,"
      - "[Théâtre National de Bretagne](https://www.t-n-b.fr/programmation/spectacles/un-apprentissage),"
      - "Soutenue par le Théâtre National de la Colline,"
actu: "Un apprentissage : après une tournée Hors les murs avec le Théâtre de la Colline puis avec l'Archipel de Fouesnant et une reprise en salle au théâtre le Colombier en 2024,le spectacle continue sa route.."
presentation:
  texte: "Voir la page dédiée sur le site de la Colline"
  lien: "https://www.colline.fr/spectacles/un-apprentissage"
dates:
  - "Tournée Hors les murs avec le Théâtre de la Colline en 23/24
    - Du 5 au 9 novembre au Théâtre Le Colombier, Bagnolet
    - Du 9 au 13 décembre à l'Archipel, Fouesnant"
---

Ce texte trouve ses origines lors d'un atelier mené dans la maison d'arrêt de Lons Le Saunier. 
Une personne, après 15 années passées en prison, vient rencontrer son enfant adolescent·e. Il revient avec ce besoin ardent de lui transmettre une chose, un questionnement essentiel à ses yeux, qu’il a mis des années à faire naître en lui : « chacun de nous a un trésor particulier en soi, qu'on doit découvrir et faire grandir, une façon de sentir, de penser, d’être au monde, personnelle, à soi et unique, comme un cadeau qui sommeille à l’intérieur... ».   

Non pas un cadeau que l’on recevrait de l’extérieur car cela peut se révéler parfois fatal...Un apprentissage est l’histoire d’une reconstruction, d’un parcours qui prend d’abord ses racines dans un terreau de croyances négatives sur soi-même menant sur des chemins toxiques et dangereux. Puis, alors qu’on se retrouve au fond du gouffre, entre les quatre murs d’une prison, une lueur d’espoir apparait grâce à l’apprentissage.   

Cet homme, illettré, entreprend d’apprendre à lire, tout seul avec L'Iliade (La Colère d’Achille). Grâce à ce livre fondateur, vieux de plus de 3 000 ans, il découvre un monde qu’il comprend, dans lequel il se reconnaît, et qui va restaurer sa confiance, son estime de soi.