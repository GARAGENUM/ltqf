---
title: "Outrages"
subtitle: "Spectacle créé le 4 novembre 2015, au Théâtre de Sartrouville, CDN"
description: "Page de présentation du spectacle Outrages de la compagnie Le Temps Qu'il Faut"
canonical: "https://ltqf.fr/spectacles/outrages/"
date: 2015-03-17T15:21:27+01:00
draft: false
credits: 
  - role: "Écriture et mise en scène"
    name: "Pierre-Yves Chapalain"
  - role: "Avec"
    name: "Jean-Louis Coulloc'h, Ludovic Le Lez, Julie Lesgages, Yann Richard, Kahena Saïghi et Catherine Vinatier"
  - role: "Collaboration artistique"
    name: "Yann Richard"
  - role: "Scénographie"
    name: "Mariusz Grygielewicz"
  - role: "Régie générale et plateau"
    name: "Fréderic Plou"
  - role: "Lumière"
    name: "Grégoire de Lafond"
  - role: "Son"
    name: "Tal Agam"
  - role: "Costumes"
    name: "Elisabeth Martin"
  - role: "Production-diffusion"
    name: "Nathalie Untersinger"
  - role: "Production déléguée" 
    name: "Compagnie Le Temps qu’il Faut"
  - role: "Co-productions et partenaires"
    name: 
      - "Les Scènes du Jura, scène nationale,"
      - "le Théâtre de Sartrouville - CDN,"
      - "le TAB, Théâtre Anne de Bretagne, scène conventionnée,"
      - "Le Canal théâtre du Pays de Redon, scène conventionnée pour le théâtre (aide à la résidence),"
      - "La Maison du Théâtre, Brest,"
      - "Le Grand Logis, ville de Bruz,"
      - "L’Archipel, pôle d’action culturelle, Fouesnant-Les Glénan,"
      - "le Théâtre Dijon-Bourgogne, CDN."
      - "Avec l’aide à la production de la DRAC Bretagne"
      - "Avec le soutien du Fonds SACD Théâtre, de la Région Bretagne, du Conseil général du Finistère et de l’ADAMI."
      - "La diffusion de ce spectacle bénéficie du soutien financier de Spectacle vivant en Bretagne."
      - "La compagnie est soutenue par la DRAC de Bretagne (Ministère de la Culture et de la Communication) au titre du conventionnement."
  - role: "Co-réalisation"
    name: "Théâtre de l’Echangeur, Bagnolet – Cie Public Chérie"
  - role: "Édition"
    name: "Outrages, l’ornière du reflux de Pierre-Yves Chapalain, est publié aux éditions Les Solitaires intempestifs"                                                                                                      
  - role: "Tournée 2015/2016"
    name: 
      - "[Théâtre de Sartrouville - CDN](http://www.theatre-sartrouville.com/evenements/outrages/),"
      - "L’Archipel, pôle d’actions culturelles - Fouesnant- Les Glénan - TAB,"
      - "Théâtre Anne de Bretagne - scène conventionnée,"
      - "Théâtre Le Grand-Logis - Ville de Bruz,"
      - "[Le Canal - théâtre du Pays de Redon - scène conventionnée](https://www.lecanaltheatre.fr/spectacle-20152016-outrages-lorniere-du-reflux),"
      - "Théâtre Dijon-Bourgogne,"
      - "Théâtre de l’Echangeur - Bagnolet,"
      - "Scènes du Jura,"
      - "Théâtre de Lons-le-Saunier."
presse:
  - revue: "Diverses revues de presse" 
---

Cette pièce raconte comment l’idée de meurtre peut germer dans la tête de gens simples confrontés à une situation d’une ambiguïté rare.Ruinés, calomniés, détruits par Edmond, Les Parents vivent depuis des années dans un extrême dénuement.  

Puis un jour, ils apprennent que cet homme veut léguer toute sa fortune à leur fille uniqueMathilde, qui vient régulièrement faire le ménage chez lui. Il n’a pas d’enfant en propre, alors c’est possible. Les parents de Mathilde interprètent son geste comme le dernier outrage qu’il peut encore leur faire.Outrages est une pièce traversée par une « vieille histoire de haine », si enracinée qu'elle façonne chaque personnage. Pour ne pas vivre coupé d'une partie vitale de soi, il appartient à chacun de tenter de tout prendre, digérer, de ne rien refouler...   

L’amour de Mathilde pour Edmond ne serait-il pas une manière de dépasser, transfigurer cette haine qui envahit tout, pour enfin vivre ?Dans Outrages, les passions contaminent peu à peu tous les personnages, dans un mondecontemporain, rural, en pleine mutation.  

Un monde en lien avec quelque chose d'archaïque mais exposé à tous les changements, aux découvertes les plus avancées telles que les manipulations génétiques ou les biotechnologies, un monde qui se métamorphose petit à petit, où la conscience s'efface parfois pour faire surgir un univers où tout devient possible...
