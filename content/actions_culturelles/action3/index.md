---
title: Hôpital psychiatrique / maison d'arrêt
description: "Page de présentation d'une action culturelle de la compagnie Le Temps Qu'il Faut dans les hopitaux psychiatriques et les maisons d'arrêt"
---

Grâce au CDN de Besançon, durant trois années consécutives, Kahena Saïghi et Pierre-Yves Chapalain ont mené un travail axé sur l’improvisation et l’expression corporelle, auprès de patients de l’hôpital psychiatrique de Novilar.  

Avec les Scènes du Jura, ils ont animé de nombreux ateliers en maison des jeunes et en milieu scolaire. Un travail d'écriture a été fait avec des détenus de la Maison d’arrêt de Lons Le Saunier.