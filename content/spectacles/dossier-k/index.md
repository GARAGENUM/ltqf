---
title: "Dossier K."
subtitle: "Spectacle créé le 25 janvier 2019 au Théâtre de l’Échangeur"
description: "Page de présentation du spectacle Dossier K de la compagnie Le Temps Qu'il Faut"
canonical: "https://ltqf.fr/spectacles/dossier-k/"
date: 2019-03-17T15:21:27+01:00
draft: false
picture: "presse/background/dossierk.jpg"
credits: 
  - role: "Texte"
    name: "Pierre-Yves Chapalain"
  - role: "Un spectacle élaboré par"
    name: "Pierre-Yves Chapalain, Laurent Gutmann et Géraldine Foucault"
  - role: "Avec les regards de"
    name: "Kahena Saïghi et Christian Giriat"
  - role: "Avec"
    name: "Pierre-Yves Chapalain et Daniel Dubois"
  - role: "Création lumière"
    name: "Florent Jacob"
  - role: "Création sonore"
    name: "Géraldine Foucault"
  - role: "Costumes"
    name: "Élisabeth Cerqueira et Kahena Saïghi"
  - role: "Construction décor"
    name: "Adeline Caron, Mohamed Elasri, Marie Hervé et Jean-Paul Rivière"
  - role: "Production"
    name: "Compagnie Le Temps qu'il Faut"
  - role: "Co-production"
    name: "La dissipation des brumes matinales"
  - role: "Accompagnement administration"
    name: "Olivier Talpaert"
  - role: "Coréalisation"
    name: "[Cie Public Chéri - théâtre de l’Echangeur, Bagnolet](https://archives.lechangeur.org/event/dossier-k/)"
  - role: "Avec le soutien de"
    name: 
      - "Studio-Théâtre de Vitry," 
      - "Théâtre de l’Archipel - Fouesnant-les Glénan,"
      - "Théâtre du Champ au Roy," 
      - "[La Chartreuse - Villeneuve lez Avignon - Centre National des Ecritures du Spectacle](https://chartreuse.org/site/dossier-k)"
      - "La compagnie est soutenue par la DRAC de Bretagne au titre du conventionnement."
presse:
  - revue: "Sceneweb"
    lien: "https://sceneweb.fr/pierre-yves-chapalain-dans-dossier-k-dapres-kafka/"
---

Dossier K. est un spectacle d’après le procès... ainsi que de quelques pensées intimes tiréesde son journal, et de certains passages du Terrier : un spectacle mettant en scène un homme« ordinaire » qui s’interroge sans cesse de manière concrète pour tenter d’élucider « la sorte de mise en accusation » dont il fait l’objet.  

Il s’agira alors pour lui (K.) de définir s’il est possible de se défendre, si toutefois « l’arrestation » dont il fait l’objet peut être prise au sérieux, car K. doute fort de la compétence des personnages qui sont venues l’importuner tôt dans sa chambre et qui n’ont rien trouver de mieux que de l’importuner au moment où il devait prendre son petit déjeuner !  

Face à un système qu’il ne comprend pas, K. cherchera à se défendre de manière farouche pour enrayer des méthodes qu’il juge arbitraire afin que cela ne se reproduise pas avec d’autres...Être accusé de quelque chose et ne pas savoir de quoi précisément on est accusé, permet defaire fonctionner l’imaginaire des spectateurs. En définitive ça permet l’ouverture d’un champ imaginaire fécond où chaque spectateur peut puiser à sa guise !