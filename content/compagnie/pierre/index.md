---
title: "Pierre-Yves Chapalain"
weight: 1
description: "Page de présentation de Pierre-Yves Chapalain, fondateur de la compagnie Le Temps Qu'il Faut"
---

Il débute comme acteur au théâtre de la main d’Or et joue dans les premiers spectacles de Joël Pommerat jusqu’à la création de Pinocchio en 2007. Acteur complice aussi de metteur.e.s en scène comme Pierre Meunier, Philippe Carbonnaux, Bérangère Ventuso, Nathalie Fillon ou Sylvain Maurice. 
Il crée la compagnie Le Temps qu’il Faut et commence à mettre en scène ses propres pièces à partir de 2008 avec La Lettre au théâtre de la Tempête, puis Absinthe au théâtre de la Bastille.  

Parallèlement à ses productions, Pierre-Yves Chapalain s’est vu confier plusieurs commandes d’écriture dont : Un heureux naufrage pour le Panta Théâtre en partenariat avec la cité de l’Immigration en 2012, Une symbiose, dans le cadre des Binômes #3, ou Une Sacrée boucherie pour IVT avec Emmanuelle Laborit en 2014.
Il a été artiste associé du CDN de Besançon, des Scènes du Jura, Du Canal théâtre de Redon.
Il écrit et met en scène également pour le jeune public, La fiancée de Barbe Bleue qui mêle ventriloquie, magie et danse et qui sera jouée plus de 150 fois.

En 2017, il présente deux autres spectacles tout public : Où sont les Ogres ? au Festival d’Avignon puis Le Secret au Très Tôt Théâtre de Quimper. 
En 2020, 2021 et 2022, avec Kahena Saïghi ils écrivent et mettent en scène Derrière tes paupières, À l’orée du bois et Un apprentissage, pièces créées et jouées, entre autres, au Théâtre National de la Colline, à la 76ème édition du Festival d’Avignon et au TNB de Rennes.
En 2023, la tournée, sur le territoire Breton, d’Un apprentissage, orchestrée par le TNB de Rennes, continue de se dérouler avec succès. Une reprise est donc prévue en Ile de France en 2024, chapeautée, cette fois-ci, par le Théâtre National de la Colline puis jouée à l'Archipel de Fouesnant.
En 2025 et 2026, deux spectacles verront le jour : Promenade en robe de chambre, en bilingue français/ breton et Dans les plis de sa langue.
