---
title: "Derrière tes paupières"
subtitle: "Le spectacle a été créé le 19 mai 2021 au Théâtre National de Bretagne-Rennes"
description: "Page de présentation du spectacle Derrière tes paupières de la compagnie Le Temps Qu'il Faut"
canonical: "https://ltqf.fr/spectacles/derriere-tes-paupieres/"
date: 2021-01-01
draft: false
credits: 
  - role: "Texte et mise en scène"
    name: "Pierre-Yves Chapalain"
  - role: "Avec"
    name: "Hiba El Aflahi, Marie Cariès, Pierre-Yves Chapalain, Pierre Giraud, Émilie Incerti Formentini, Kahena Saighi, Nicolas Struve"
  - role: "Dramaturgie"
    name: "Kahena Saighi"
  - role: "Collaboration artistique"
    name: "Jonathan Le Bourhis"
  - role: "Lumière"
    name: "Florent Jacob"
  - role: "Son"
    name: "Samuel Favart-Mikcha"
  - role: "Scénographie et costumes"
    name: "Adeline Caron"
  - role: "Aide à la réalisation des costumes"
    name: "Myriam Rault et Julia Brochier"
  - role: "Construction du décor"
    name: "Gaël Richard"
  - role: "Maquillages et coiffures"
    name: "Mathilde Benmoussa et Anne Binois"
  - role: "Régie générale"
    name: "Andréa Warzée"
  - role: "Production"
    name: "Compagnie Le Temps qu’il Faut"
  - role: "Co-production"
    name: 
      - "[La Colline – théâtre national](https://www.colline.fr/spectacles/derriere-tes-paupieres),"
      - "[Théâtre National de Bretagne-Rennes](https://www.t-n-b.fr/programmation/derriere-tes-paupieres),"
      - "Les Quinconces-L’Espal -Scène nationale du Mans,"
      - "Château Rouge – Scène conventionnée d'Annemasse"
      - "Avec le soutien du Studio-Théâtre de Vitry et la participation artistique de l’ENSATT"
      - "La compagnie est soutenue par la DRAC de Bretagne– ministère de la Culture au titre du conventionnement et la région Bretagne."
  - role: "Administration de production et diffusion" 
    name: "Olivier Talpaert – En votre compagnie"
  - role: "Édition"
    name: "Derrière tes paupières est édité aux éditions Les solitaires intempestifs"
---

Éléonore, la quarantaine, est au bord de l’épuisement. Depuis quelque temps, elle a des oublis de plus en plus fréquents et des problèmes d’élocution. Alors elle note compulsivement ses souvenirs sur des post-it et décide de consulter un neurologue qui lui propose, à titre expérimental, une « aide technique », une sorte d’être hybride mi-végétal mi-humain, pour surveiller sa santé en direct.  

Éléonore refuse et son état empire : ses phrases sont décousues, elle perd le fil des conversations, puis les mots, avant d’arrêter complètement de parler. Sa sœur Maya, qu’elle n’avait pas revue depuis des années, accepte alors la proposition du neurologue. L’humanoïde traduira les pensées d’Éléonore et les exprimera à sa place.  

Qu’est-ce qui résiste à l’oubli ? Comment retrouver la parole perdue ? Quelle est notre relation au langage et à la pensée face aux nouvelles technologies ? Sont-elles un soutien ou une entrave ?  
