---
title: "Dans les plis de sa langue"
subtitle: "Création en 2026"
description: "Page de présentation du spectacle Dans les plis de sa Langue de la compagnie Le Temps Qu'il Faut"
canonical: "https://ltqf.fr/spectacles/dans-les-plis-de-sa-langue/"
date: "2026-01-01"
draft: false
actu: "Dans les plis de sa langue : après la présentation de maquette au Théâtre du Rond-Point en octobre 24, sa création est prévue à l'automne 2026 "
credits:
  - role: "Écriture et mise en scène"
    name: "Pierre-Yves Chapalain et Kahena Saïghi"
  - role: "Avec"
    name: "Marie Cariès, Kahena Saïghi, Pierre-Yves Chapalain, Miléna Sansonetti et Baptiste Znemenak"
  - role: "Assistant à la mise en scène"
    name: "Eric Feldman"
  - role: "Création lumière"
    name: "Thibault Moutin"
  - role: "Création sonore"
    name: "Samuel Favart-Mikcha"
  - role: "Conseil scénographie et costumes"
    name: "Andréa Warzée"
  - role: "Administration de production"
    name: "Thomas Clédé"
  - role: "Production"
    name: "Compagnie Le Temps qu'il Faut, recherche de partenaires en cours..."
  - role: "Résidence"
    name: "Le Cube - Hérisson"
---

C'est une histoire qui parle de nos confiances que l'on blesse trop souvent, qu'on ne laisse même pas germer dans notre esprit parce qu'on intègre trop vite l'idée que les rêves sont pour les autres...
L'histoire : Emma, une jeune fille à peine sortie de l'adolescence, vit avec sa mère et son oncle paternel, au dessus du restaurant familial. L'oncle, le frère jumeau de son père décédé et qui fût une sorte de tyran domestiqe, est chef cuistot et tient le restaurant familial accompagné de son jeune apprenti Alan, ancien ami d'enfance d'Emma.

Ce dernier, au parcours chaotique, a pu écourter sa peine de prison, gràce à l'oncle et aux ateliers de cuisine qu'il dispensait au sein de l'établissement pénitencier. En effet, l'oncle a reconnu en Alan un don extraordinaire pour la cuisine. D'ailleurs, la mère d'Emma qui refusait de s'alimenter, se remet à manger depuis qu'il lui concocte ses repas.
Emma reçoit une lettre d'admission à une école prestigieuse, à laquelle elle a candidaté sans véritablement y croire, persuadée que ce genre d'école n'est pas fait pour elle et défiante vis-à-vis de ce type d'insitution qui, selon elle, formate et uniformise la pensée. 

Son oncle s'attache à  l'idée de la voir partir étudier dans cet établissement renommé. Pour lui, cela représente l'accession à un monde nouveau, plein de promesses. Il ne comprend pas l'hésitation de sa nièce. 

Elle, ne comprend pas son insistance. Pourquoi veut-il se débarasser d'elle ? Emma pense à sa mère, qui semble perdre la mémoire, comme vidée de sa substance vitale, elle l'estime trop vulnérable pour la laisser seule avec son oncle. Elle l'a tellement vu se faire dévorer par son père, elle craint que l'histoire se répète avec son oncle. En effet, celui-ci tente, depuis la mort de son frère jumeau, de rappeler à sa belle-soeur les souvenirs d'une relation amoureuse passée et toujours restée secrète, une manière de la ramener dans le flux de la vie. 

Tout ce petit monde qui tente d'éclore à nouveau pourra-t-il s'affranchir de l'aura dévastatrice du défunt mari?