---
title: "Où sont les ogres ?"
subtitle: "Spectacle tout public à partir de 10 ans, créé au Festival IN d’Avignon 2017"
description: "Page de présentation du spectacle Où sont les ogres de la compagnie Le Temps Qu'il Faut"
canonical: "https://ltqf.fr/spectacles/ou-sont-les-ogres/"
date: 2017-03-17T15:21:27+01:00
draft: false
credits:
  - role: "Texte et mise en scène"
    name: "Pierre-Yves Chapalain"
  - role: "Avec"
    name: "Jean-Louis Coulloc’h, Boutaïna El Fekkak, Julie Lesgages, Catherine Vinatier"
  - role: "Collaboration artistique"
    name: "Yann Richard"
  - role: "Scénographie"
    name: "Éric Soyer assisté de Marie Hervé"
  - role: "Lumière"
    name: "Éric Soyer assisté de Thibault Moutin"
  - role: "Composition sonore"
    name: "Géraldine Foucault"
  - role: "Costumes"
    name: "Elisabeth Cerqueira"
  - role: "Régie générale, régie plateau et collaboration à la construction"
    name: "Frédéric Plou"
  - role: "Administration de production et diffusion"
    name: "Nathalie Untersinger"
  - role: "Production"
    name: "Compagnie Le temps qu’il faut"
  - role: "Co-production"
    name: 
      - "ExtraPôle Provence-Alpes-Côte d’Azur,"
      - "Théâtre de Lorient Centre dramatique national,"
      - "[Festival d’Avignon](https://festival-avignon.com/fr/edition-2017/programmation/ou-sont-les-ogres-7251),"
      - "Le Canal Théâtre du Pays de Redon Scène conventionnée pour le théâtre,"
      - "Maison du Théâtre de Brest,"
      - "L’Archipel Pôle d’action, culturelle Fouesnant-Les Glénan,"
      - "Les Scènes du Jura Scène nationale,"
      - "Théâtre du Champ du Roy à Guingamp."
      - "Avec le soutien de la Drac Bretagne, Région Bretagne, conseil départemental du Finistère."
      - "Avec l’aide du Très Tôt Théâtre Scène conventionnée jeunes publics de Quimper et du Studio-Théâtre de Vitry."
      - "Résidence Ferme du Buisson Scène nationale de Marne-la-Vallée"
  - role: "Édition"
    name: "Où sont les ogres ? de Pierre-Yves Chapalain est publié aux éditions Les Solitaires Intempestifs"
  - role: "Tournée 2017/2018"
    name: 
      - "Théâtre de Lorient - Centre dramatique national,"
      - "Le Canal Théâtre du Pays de Redon - Scène conventionnée pour le théâtre,"
      - "Maison du Théâtre de Brest,"
      - "L’Archipel - Pôle d’action culturelle à Fouesnant-Les Glénan,"
      - "Les Scènes du Jura - Scène nationale,"
      - "Théâtre du Champ du Roy à Guingamp"
presse:
  - revue: "Télérama"  
---

De nos jours, dans une grande ville, une femme vit seule avec sa fille, Hannah. Celle-ci ne sort plus de sa chambre. Tiraillée par d'étranges envies qui la mettent mal à l'aise et l'éloignent des autres, elle n'a d'yeux que pour celle qu'elle n'a jamais vue mais qui la comprend comme une sœur : Angelica. Elles discutent jour et nuit sur Internet.   

Et pendant les sommes que pique Angelica à tout bout de champ et qui peuvent durer un certain temps, Hannah guette ses écrans et angoisse à l'idée que son amie l'ait oubliée. La mère d'Hannah consulte un ami médecin : sommes-nous bien sûrs qu'Hannah ne discute pas avec une intelligence artificielle ? Il faut réagir, sortir, se divertir....   

Le patron du grand restaurant voisin a invité un cirque ; excellente occasion pour Hannah de faire montre d'accepter un peu de sociabilité... d'autant que la fille du patron, va-t-elle découvrir, n'est autre qu'Angelica. Une fois réunies à la campagne, en chair et en os, les deux adolescentes partagent leurs secrets et leur nature particulière...   

Auteur et metteur en scène Pierre-Yves Chapalain mêle rêve, magie et virtualité pour explorer les instincts naissants des jeunes filles. Ravageurs ou créateurs ?
