---
title: "La lettre"
subtitle: "Spectacle créé à l’automne 2008 au Théâtre de la Tempête à Paris"
description: "Page de présentation du spectacle La Lettre de la compagnie Le Temps Qu'il Faut"
canonical: "https://ltqf.fr/spectacles/la-lettre/"
date: "2008-03-17T15:21:27+01:00"
draft: false
credits:
  - role: "Écriture et mise en scène"
    name: "Pierre-Yves Chapalain"
  - role: "Collaboration"
    name: "Yann Richard, Ludovic Le Lez"
  - role: "Avec"
    name: "Patrick Azam, Philippe Frécon, Perrine Guffroy, Laure Guillem, Yann Richard, Airy Routier, Catherine Vinatier, Margaret Zenou"
  - role: "Scénographie et costumes"
    name: "Marguerite Bordat, Anne-Sophie, Germinal Sauget"
  - role: "Maquillage et coiffure"
    name: "Nathalie Régior"
  - role: "Musique et son"
    name: "Frédéric Lagnau"
  - role: "Créations lumières"
    name: "Gilles David et Catherine Verheyde"
  - role: "Production"
    name: "Compagnie Le Temps qu'il Faut"
  - role: "Co-production"
    name: 
      - "Théâtre de la Coupe d'or - scène conventionnée de Rochefort,"
      - "avec le soutien d'Arcadi"
      - "En coréalisation avec le [Théâtre de la Tempête](https://www.la-tempete.fr/saison/2008-2009/spectacles/la-lettre-394)"
      - "Partenaire Nouveau théâtre de Besançon-Franche Comté CDN"
  - role: "Édition"
    name: "La lettre de Pierre-Yves Chapalain, est publié aux éditions Les Solitaires intempestifs"
presse:
  - revue: "Diverses revues de presse"  
---

Un Père (il va mourir) réunit autour de lui les personnes qui lui sont le plus cher.Comme une prémonition, ce « mal être » du Père annonce quelque chose de plus trouble : l’arrivée de son frère (oublié) « qui rien que par sa présence a causé déjà tellement de troubles »... Une lettre est adressée à sa Femme.

Une lettre qui ne contient pas un contenu précis, puisqu’elle est écrite en une langue inconnue de tous... Les suppositions vont bon train, tous sont mis à nu, labourés intérieurement par ces périodes troubles, tout ce passé violent qui semble prêt à refaire surface. Dans les hypothèses évoquées, la plus terrible (pour le mari surtout) semblerait être le retour de William...Qui est donc ce William pour susciter tant de mouvements passionnels chez chacun des personnages ? Une horreur surgira-t-elle ?

Non, puisque les personnes de la pièce en question semblent maîtriser la situation.

Alors quoi? Ça gratouille du côté du mythe de Dionysos (un peu)... Ce William dont tout le monde parle est capable de frapper de démence les personnes qu'il rencontre...

Cela gratouille aussi du côté du mythe de Thyeste ! Oui... car une jalousie féroce tiraille l'un des personnages... L'air de rien...
