---
title: "Le secret"
subtitle: "Spectacle tout public, à partir de 5 ans, créé le 8 octobre 2017 au festival Théâtre à Tout Âge"
description: "Page de présentation du spectacle Le Secret de la compagnie Le Temps Qu'il Faut"
canonical: "https://ltqf.fr/spectacles/le-secret/"
date: "2017-10-08"
draft: false
credits: 
  - role: "Écriture et mise en scène"
    name: "Pierre-Yves Chapalain en collaboration avec les acteurs"
  - role: "Avec"
    name: "Kahena Saïghi, Christophe Hanon et Mélanie Bouvret"
  - role: "Collaboration à la scénographie"
    name: "Mariusz Grygielewicz"
  - role: "Régie générale"
    name: "Mélanie Bouvret"
  - role: "Administration de production et diffusion"
    name: "Nathalie Untersinger"
  - role: "Production déléguée"
    name: "Compagnie Le Temps qu’il Faut"
  - role: "Co-producteurs"
    name: 
      - "[Le Canal - Théâtre du pays de Redon - Scène conventionnée pour le théâtre](https://www.lecanaltheatre.fr/spectacle-20172018-le-secret),"
      - "Très tôt théâtre - scène conventionnée jeune public"
      - "La compagnie est soutenue par la DRAC de Bretagne (Ministère de la culture et de la communication) au titre du conventionnement"
      - "Avec le soutien de la Chartreuse-Villeneuve-Lez-Avignon, Centre national des écritures du spectacle et de l’EREA-établissement régional d’enseignement adapté de Redon."
  - role: "Tournée 2017/2018"
    name: 
      - "Le Canal Théâtre du pays de Redon - Scène conventionnée pour le théâtre,"
      - "Festival Théâtre Á Tout Âge,Très tôt théâtre - scène conventionnée jeune public"


---

Léna a 7 ans et un secret. Elle aimerait le partager avec son copain Ulysse, mais il lui faut d’abord être sûre de leur amitié. Quand enfin elle lui avoue qu’elle est une ogresse, Ulysse, pourtant pas tout à fait rassuré, lui propose de l’aider.   

Mais comment guérir de "l’ogreté" ? Un pantin découvert au fond d’une vieille valise leur donnera quelques pistes, mais sa mémoire est bien défaillante… Léna et Ulysse vont s’engager dans une quête qui mettra à l’épreuve leur amitié : grandir, ce n’est pas si facile…   

Pour raconter cette quête : deux comédiens, un pantin qui prend vie magiquement grâce à la ventriloquie, un ogre, un cerf, des "objets vivants" ; comme cette chaise en bois qui souhaite retourner à l’état sauvage… et un travail sonore et musical pour les accompagner.  

Le Secret vient en écho à Où sont les ogres ?
