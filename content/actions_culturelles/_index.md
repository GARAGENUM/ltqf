---
title: "Actions culturelles"
date: 2023-03-17T15:21:27+01:00
draft: false
picture: "/apropos.png"
subtitle: "Quelques actions réalisées par la compagnie"
description: "Page de présentation des actions culturelles de la compagnie"
canonical: "https://ltqf.fr/actions_culturelles/"
---

Depuis sa création, la compagnie le temps qu’il faut, en partenariat avec différents lieux, propose des ateliers d’écriture et de pratique théâtrale à des publics aussi variés que possible : des élèves de la maternelle au lycée (Classique, SEGPA, dispositif ULIS, UPE2A), des jeunes "décrocheurs", des groupes parents/ enfants, des chômeurs longue durée, des personnes suivies en psychiatrie…
