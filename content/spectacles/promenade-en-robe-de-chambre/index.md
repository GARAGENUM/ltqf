---
title: "Promenade en robe de chambre"
subtitle: "Spectacle tout-terrain, Création janvier 2025 en région Bretagne"
description: "Page de présentation du spectacle Promenade en Robe de Chambre de la compagnie Le Temps Qu'il Faut"
canonical: "https://ltqf.fr/spectacles/promenade-en-robe-de-chambre/"
date: "2024-01-01"
draft: false
picture: ""
actu: "Promenade en robe de chambre : spectacle tout terrain. Création le 31 janvier au théâtre de Morlaix et le 9 février à l'espace Armorice/Plouguerneau"
credits:
  - role: "Écriture et mise en scène"
    name: "Pierre-Yves Chapalain et Kahena Saïghi"
  - role: "Avec"
    name: "Jean-Yves Gourves, Alan Roignant et Norma Suzanne Conrath (contrebasse)"
  - role: "scénographie/création lumière et régie générale"
    name: "Thibault Moutin"
  - role: "Administration, production"
    name: "Thomas Clédé"
  - role: "Production"
    name: "Compagnie Le Temps qu’il Faut"
dates:
  - "Saison 2025/2026":
    - "Le 31 Janvier, Théâtre de Morlaix"
    - "Le 9 février à l'Armorica, Espace Culturel de Plouguerneau"
---

Un homme d’un certain âge, écrit une lettre à sa nièce Josepha, qu’il a élevé comme sa fille. Il s’inquiète pour elle depuis qu’un ami lui a dit qu’elle aurait des problèmes avec la justice…Au bout de la troisième lettre sans réponse, et de plus en plus inquiet, l’oncle décide de faire le voyage pour se rendre chez ssa nièce.  

En arrivant, l’appartement semble vide. La chambre à coucher est fermée à clé. Josepha est-elle enfermée dedans ? Aucun bruit, ni signe de vie ne semble le confirmer. A-t-elle fui ? A-t-elle été arrêtée par les autorités ? L’oncle va chercher à comprendre la cause de ce mystère.
