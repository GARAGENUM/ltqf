---
title: "Brume du soir"
subtitle: "Spectacle créé en février 2014 au Théâtre Anne de Bretagne, Vannes"
description: "Page de présentation du spectacle Brume du Soir de la compagnie Le Temps Qu'il Faut"
canonical: "https://ltqf.fr/spectacles/brume-du-soir/"
date: "2014-03-17T15:21:27+01:00"
draft: false
credits:
  - role: "Écriture et mise en scène"
    name: "Pierre-Yves Chapalain"
  - role: "Avec"
    name: "Eric Challier, Antek Klemm, Kahena Saïghi"
  - role: "Collaboration artistique"
    name: "Yann Richard"
  - role: "Création lumière"
    name: "Grégoire de Lafond"
  - role: "Création son"
    name: "Tal Agram"
  - role: "Régie plateau"
    name: "Frédéric Plou"
  - role: "Collaboration à la scénographie et aux costumes"
    name: "Pia de Compiègne"
  - role: "Marionnette réalisée par"
    name: "Marguerite Bordat"
  - role: "Voix"
    name: "Odja Llorca"
  - role: "Administration de production et diffusion"
    name: "Nathalie Untersinger"
  - role: "Production"
    name: "Compagnie Le Temps qu’il Faut et Théâtre Dijon Bourgogne"
  - role: "Co-production"
    name: 
      - "Théâtre Anne de Bretagne, scène conventionnée – Vannes,"
      - "l’Archipel, pôle d’action culturelle de Fouesnant-Les Glénan - Fouesnant"
      - "Les Théâtrales Charles Dullin – Festival de la création contemporaine,"
      - "Théâtre Edwige Feuillère, scène conventionnée de Vesoul,"
      - "Théâtre de Poche d’Hédé"
      - "Avec le soutien du Ministère de la Culture et de la Communication au titre de l'aide à la production,"
      - "Avec le soutien de la Région Bretagne et de Spectacle Vivant en Bretagne."
  - role: "Co-réalisation"
    name: "Théâtre de l’Echangeur, Bagnolet – Cie Public Chérie"
  - role: "Tournée 2014/2015"
    name: 
      - "Théâtre Anne de Bretagne, scène conventionnée – Vannes,"
      - "l’Archipel, pôle d’action culturelle de Fouesnant-Les Glénan - Fouesnant,"
      - "Théâtre de l’Echangeur – Paris,"
      - "Théâtre Dijon-Bourgogne, centre dramatique national,"
      - "La Faïencerie-Théâtre – Creil,"
      - "Théâtre Edwige Feuillère, scène conventionnée – Vesoul,"
      - "[Théâtre de Poche de Hédé – Hédé-Bazouges](https://theatre-de-poche.com/la-brume-du-soir),"
      - "Scènes du Jura, scène nationale,"
      - "Théâtre de Morlaix"
presse:
  - revue: "Diverses revues de presse"  
    lien: "presse1.pdf"
---

Monsieur Alexandra est fou d'inquiétude. Sa fille, Mathilde, n’est pas rentrée depuis plusieurs jours… Il n'est pourtant pas du genre à empêcher sa fille de vivre sa vie, il aime trop la liberté pour lui interdire quoique ce soit.  

Mais il trouve qu'elle a depuis quelque temps un comportement étrange, il sent l'imminence d'une catastrophe au plus profond de lui-même… C'est peut-être à cause de cet homme dont Mathilde s'est entichée, avec qui elle discute le soir au bord du fleuve. Il est jeune, mais il semble si vieux à Monsieur Alexandra, et ravive en lui de douloureux souvenirs d'un pays lointain, d'une vie passée jusque-là soigneusement cachée.   

La Brume du soir est une histoire d'exils. D'exils réels, ceux du père et de l'homme, qui ont fui un pays lointain pour se reconstruire ailleurs. D'exil intérieur aussi, cette perte de l'âme que peut provoquer l'effacement du passé, l'oubli des origines. Le père veut être « géographique », se développer en fonction du lieu où il vit. Il fait table rase du passé pour ne pas se laisser entraver par quoique ce soit de négatif, comme une plante qui doit s’adapter coûte que coûte au lieu où elle se trouve…Mais quelque chose vient toquer à la porte de sa conscience, quelque chose qui n’est peut-être pas le passé, mais tout simplement son âme qui cherche à ne pas mourir…  

La Brume du soir est une histoire d'amour. Un amour dangereux, interdit, mystérieux aussi puisque les deux amants ne parviennent pas à se souvenir des mots qui les ont rapprochés, aimantés. Ces mots sont entrés dans l'oreille de Mathilde et l'ont touchée au plus profond d'elle-même, l'ont conduit comme « à la lisière d'une forêt ». Des mots qui, comme chez les Dogons, nourrissent vraiment et habillent de vêtements brodés celui qui les reçoit dans l’oreille…
