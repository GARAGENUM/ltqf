---
title: "L'Orée du bois"
subtitle: "Création le 7 juillet 2022 au festival IN d’Avignon"
description: "Page de présentation du spectacle L'Orée du Bois de la compagnie Le Temps Qu'il Faut"
canonical: "https://ltqf.fr/spectacles/oree-du-bois/"
date: 2022-07-22
draft: false
credits: 
  - role: "Écriture"
    name: "Pierre-Yves Chapalain"
  - role: "Mise en scène"
    name: "Pierre-Yves Chapalain et Kahena Saïghi"
  - role: "Collaborateur artistique"
    name: "Jonathan Le Bourhis"
    name: "Avec"
    name: "Madeleine Louarn, Kahena Saïghi, Pierre-Yves Chapalain, Pablo Pensavalle"
  - role: "Création musicale et sonore"
    name: "Pablo Pensavalle"
  - role: "Co-production" 
    name: "Espace des Arts-scène nationale Chalon-sur-Saône, [Festival d’Avignon](https://festival-avignon.com/fr/edition-2022/programmation/a-l-oree-du-bois-190968)"
presse:
  - revue: "Vaucluse et Le Monde" 
    lien: "vaucluse.pdf"
---

Un couple de citadins, vient de s'installer sur les terres familiales, à l'orée d'un bois. Sorte de néo-ruraux qui ont fait le pas d'un nouveau départ, pour une vie différente, plus libre peut-être, dans un contexte où les interdits sont de plus en plus nombreux...  

Le mari, totalement novice dans le domaine de la terre et des lois élémentaires, écoute les conseils avisés de la Coryphée, avec laquelle il sympathise. Celle-ci est le personnage par lequel se raconte l'histoire, lors de ses rondes nocturnes, à la recherche du mystérieux organisateur de fêtes sauvages impossibles à localiser.  

À l’Orée du bois, un oratorio où la musique laisse de l’espace à la narration, et où la maire du village vient au-devant de nous, dévoiler contexte, récit, personnages et motivations: tout ce que nous avons besoin de savoir pour nous faire imaginer.
