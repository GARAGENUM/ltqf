---
title: "La fiancée de Barbe bleue"
subtitle: "Spectacle tout public à partir de 8 ans créé en janvier 2010 en Franche Comté - Réseau Côté Court"
description: "Page de présentation du spectacle La Fiancée de Barbe Bleue de la compagnie Le Temps Qu'il Faut"
canonical: "https://ltqf.fr/spectacles/fiancee-de-barbe-bleue/"
date: "2010-03-17T15:21:27+01:00"
draft: false
credits:
  - role: "Écriture et mise en scène"
    name: "Pierre-Yves Chapalain"
  - role: "Avec"
    name: "Pierre-Yves Chapalain ou Philippe Frécon et Kahena Saïghi"
  - role: "Collaboration artistique"
    name: "Yann Richard"
  - role: "Scénographie, costumes et marionnette"
    name: "Marguerite Bordat"
  - role: "Musique et son"
    name: "Frédéric Lagnau"
  - role: "Collaboration ventriloque"
    name: "Michel Dejeneffe"
  - role: "Lumières et régie générale"
    name: "Grégoire de Lafond"
  - role: "Co-production"
    name: 
      - "Compagnie Le Temps qu’il Faut,"
      - "Nouveau Théâtre - Centre dramatique national de Besançon et de Franche-Comté"
  - role: "Co-réalisation"
    name: "[Théâtre de l’Échangeur](https://archives.lechangeur.org/event/fiancee-de-barbe-bleue/)"
  - role: "Tournée 2010-2013"
    name: 
      - "Théâtre de la coupe d’or (Rochefort),"
      - "Festival Oups (Brest),"
      - "Nouveau Théâtre CDN (Besançon),"
      - "Théâtre de L'Echangeur (Bagnolet),"
      - "le réseau Côté-Cour de Franche-Comté,"
      - "Act'Art 77 (décentralisation en Seine et Marne),"
      - "[Théâtre Gérard Philipe Festival 'Et moi alors?'](https://www.theatregerardphilipe.com/tgp-cdn/spectacle/la-fiancee-de-barbe-bleue.html),"
      - "Théâtre Edwige Feuillère (Vesoul),"
      - "Festival Dedans-Dehors (Bretigny),"
      - "tournée estivale dans les centres CCAS,"
      - "Le Forum, scène conventionnée (Blanc-Mesnil),"
      - "Très Tôt Théâtre-Théâtre Max Jacob (Quimper),"
      - "L'Espal centre culturel-Théâtre Paul Scarron (Le Mans),"
      - "Théâtrales Charles Dullin - Théâtre de Charenton le Pont,"
      - "L’Hectare, scène conventionnée de Vendôme,"
      - "Théâtre de Laval,"
      - "Espace Jacques Prévert, Aulnay-sous-bois,"
      - "Transversales, Verdun (en décentralisation),"
      - "Centre dramatique national d’Orléans,"
      - "La Faïencerie – Théâtre de Creil"
presse:
  - revue: "Diverses revues de presse"  
---

Gilles et Marguerite viennent de se fiancer. Ils emménagent dans une grande maison au bord de l’océan. Le couple semble heureux. Gilles veut refaire sa vie, retrouver sa jeunesse, « tout remettre à plat ». Quand il aura « tout remis à neuf », ils pourront se marier.  

Mais ses pertes de mémoire l’inquiètent et Gilles s’absente pour passer des radios de « l’intérieur de son crâne ». Il confie à Marguerite les clefs de la maison, en lui précisant de ne pas utiliser la petite clef qui ouvre la porte d’une grosse boîte…  

Marguerite s’ennuie. Elle joue avec Jean, la marionnette de Gilles. Quand Marguerite découvre la boîte interdite, Jean la pousse à l’ouvrir. Ce qu’elle voit à l’intérieur l’horrifie.
Gilles rentre de très bonne humeur. Mais il découvre que Marguerite a ouvert la boîte. Il y enferme Marguerite et Jean, bien décidé à les laisser mourir.  

Heureusement, une loutre vient les délivrer. Pour éviter que Gilles ne les poursuive, elle place une poule dans la boîte. Grâce au pouvoir enivrant de la musique de l’océan, Gilles prendra cette poule pour sa fiancée. Il passera le reste de sa vie à lui parler, à lui demander pardon, en attendant qu’elle lui réponde.

